package libs

import (
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/stretchr/testify/assert"
)

func ValidateS3(t *testing.T, awsRegion string, bucketName string) {
	c := aws.NewS3Client(t, awsRegion)
	t.Run("bucketExists", func(t *testing.T) {
		output, err := c.ListBuckets(nil)
		if err != nil {
			assert.Fail(t, fmt.Sprintf("Failed to get buckets: %s", err.Error()))
		}
		var names []string
		for _, element := range output.Buckets {
			names = append(names, *element.Name)
		}
		assert.Contains(t, names, bucketName)
	})
	t.Run("bucketTagged", func(t *testing.T) {
		output, err := c.GetBucketTagging(&s3.GetBucketTaggingInput{
			Bucket: &bucketName,
		})
		if err != nil {
			assert.Fail(t, fmt.Sprintf("Failed to get bucket tags: %s", err.Error()))
		}
		var tags []map[string]string
		for _, element := range output.TagSet {
			tags = append(tags, map[string]string{*element.Key: *element.Value})
		}
		assert.Contains(t, tags, map[string]string{"Name": bucketName})
		assertMandatoryTagsExists(t, tags)
	})
}
