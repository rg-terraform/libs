package libs

import (
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/service/elasticsearchservice"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/stretchr/testify/assert"
)

func ValidateES(t *testing.T, awsRegion string, domain string) {
	sess, err := aws.NewAuthenticatedSession(awsRegion)
	t.Run("domainExist", func(t *testing.T) {
		if err != nil {
			assert.Fail(t, fmt.Sprintf("Failed to create session: %s", err.Error()))
		}
		c := elasticsearchservice.New(sess)
		_, err = c.DescribeElasticsearchDomain(&elasticsearchservice.DescribeElasticsearchDomainInput{
			DomainName: &domain,
		})
		if err != nil {
			assert.Fail(t, fmt.Sprintf("Failed to get elasticsearch domain: %s", err.Error()))
		}
		assert.True(t, true, "Domain exist")
	})
}
