package libs

import (
	"testing"

	"github.com/aws/aws-sdk-go/service/cloudwatchlogs"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/stretchr/testify/assert"
)

func ValidateCloudwatch(t *testing.T, awsRegion string, logGroupName string, logStreamName string) {
	c := aws.NewCloudWatchLogsClient(t, awsRegion)
	t.Run("logGroupExists", func(t *testing.T) {
		output, err := c.DescribeLogGroups(&cloudwatchlogs.DescribeLogGroupsInput{})
		if err != nil {
			assert.Fail(t, "Failed to get log groups")
		}
		var names []string
		for _, element := range output.LogGroups {
			names = append(names, *element.LogGroupName)
		}
		assert.Contains(t, names, logGroupName)
	})
	t.Run("logGroupTagged", func(t *testing.T) {
		output, err := c.ListTagsLogGroup(&cloudwatchlogs.ListTagsLogGroupInput{
			LogGroupName: &logGroupName,
		})
		if err != nil {
			assert.Fail(t, "Failed to get log groups tags")
		}
		var tags []map[string]string
		for key, value := range output.Tags {
			tags = append(tags, map[string]string{key: *value})
		}
		assertMandatoryTagsExists(t, tags)
	})
	t.Run("logStreamExists", func(t *testing.T) {
		output, err := c.DescribeLogStreams(&cloudwatchlogs.DescribeLogStreamsInput{
			LogGroupName: &logGroupName,
		})
		if err != nil {
			assert.Fail(t, "Failed to get log streams")
		}
		var names []string
		for _, element := range output.LogStreams {
			names = append(names, *element.LogStreamName)
		}
		assert.Contains(t, names, logStreamName)
	})
}
