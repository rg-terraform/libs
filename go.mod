module gitlab.com/rg-terraform/libs

go 1.12

require (
	github.com/aws/aws-sdk-go v1.20.15
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gruntwork-io/terratest v0.17.5
	github.com/pquerna/otp v1.2.0 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
)
