package libs

import (
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/stretchr/testify/assert"
)

func ValidateLambda(t *testing.T, awsRegion string, lambdaName string) {
	sess, err := aws.NewAuthenticatedSession(awsRegion)
	if err != nil {
		assert.Fail(t, fmt.Sprintf("Failed to create session: %s", err.Error()))
	}
	c := lambda.New(sess)
	t.Run("functionExists", func(t *testing.T) {
		output, err := c.GetFunction(&lambda.GetFunctionInput{
			FunctionName: &lambdaName,
		})
		if err != nil {
			assert.Fail(t, fmt.Sprintf("Failed to get lambda function: %s", err.Error()))
		}
		assert.NotEqual(t, output.Configuration, nil)
	})
	t.Run("functionTagged", func(t *testing.T) {
		output, err := c.GetFunction(&lambda.GetFunctionInput{
			FunctionName: &lambdaName,
		})
		if err != nil {
			assert.Fail(t, fmt.Sprintf("Failed to get lambda function: %s", err.Error()))
		}
		var tags []map[string]string
		for key, value := range output.Tags {
			tags = append(tags, map[string]string{key: *value})
		}
		assert.Contains(t, tags, map[string]string{"Name": lambdaName})
		assertMandatoryTagsExists(t, tags)
	})
}
