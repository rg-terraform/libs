package libs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func assertMandatoryTagsExists(t *testing.T, tags []map[string]string) {
	var keys []string
	for _, e := range tags {
		for key, _ := range e {
			keys = append(keys, key)
		}
	}
	t.Parallel()
	t.Run("qventus:customer", func(t *testing.T) { assert.Contains(t, keys, "qventus:customer") })
	t.Run("qventus:environment", func(t *testing.T) { assert.Contains(t, keys, "qventus:environment") })
	t.Run("qventus:stack", func(t *testing.T) { assert.Contains(t, keys, "qventus:stack") })
	t.Run("role", func(t *testing.T) { assert.Contains(t, keys, "role") })
	t.Run("owner:team", func(t *testing.T) { assert.Contains(t, keys, "owner:team") })
}
